## Ansible. Deploy the application.

## Step 1: clone git repository

  ```
  git clone https://gitlab.com/glad2rest/ansible_production.git
  ```

## Step 2: install the ansible package

- For Debian/Ubuntu:
  ```sudo apt-get install ansible```
- For Red Hat/CentOS:
  ```sudo yum install ansible```
- For Fedora
  ```sudo dnf install ansible```
- For openSUSE:
  ```sudo zypper install ansible```
- For Arch Linux:
  ```sudo pacman -S ansible```
- For macOS:
  ```brew install ansible```

## Step 3: Prelaunch

Once the ansible is installed you should provide your credentials to set the connection to your virtual or local machine.
  ```
  cd ansible_production && \
  ansible-vault edit inventories/host_vars/local_host.yml \
  --vault-password-file=inventories/host_vars/pass
  ```
![credentials](img/credentials.png)

## Step 4: Launch
After the credentials are set execute the command below to run a script to mount and run the application on a virtual or local machine.
  ```
  ansible-playbook deploy.yml -i inventories/vm_host.yml \
  --vault-password-file=inventories/host_vars/pass
  ```

## Step 5: Troubleshoot
If you get an error during the connection establishment stage - make sure you entered valid data in the inventories/vm_host.yml file
 (step 3)
![unreachable](img/unreachable.png)
